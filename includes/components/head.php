<head>
    <meta charset="UTF-8">
    <title>
        <?php wp_title(); ?>
    </title>
    <meta name="description" content="Studio Mobile - Excelência em Móveis Planejados">
    <meta name="language" content="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="">
    <meta name="language" content="pt-br" />
    <link rel="canonical" href="<?= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>" />
    <?php wp_head(); ?>

<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MS2QH92');</script>
<!-- End Google Tag Manager -->


</head>
<header>
    <div class="container-fluid top-items">
        <div class="row">
            <div class="col-12 col-lg-10">
                <div class="items-top">
                    <img class="icon-top" src="<?php echo get_template_directory_uri(). '/images/icons/icon-local.png'; ?>" alt="Localização">
                    <p class="text-top">Av. Bosque da Saúde 1.061, Sala 101 - Vila da Saúde | São Paulo / SP</p>
                    <img class="icon-top" src="<?php echo get_template_directory_uri(). '/images/icons/icon-zap.png'; ?>" alt="Whatsapp">
                    <a target="_blank" href="https://api.whatsapp.com/send?phone=551144101253&text=&source=&data=" class="text-top">(11) 4410.1253</a>
                    <img class="icon-top" src="<?php echo get_template_directory_uri(). '/images/icons/icon-phone.png';?>" alt="Telefone">
                    <a href="tel:+551144101253" class="text-top">(11) 4410.1253</a>
                    <img class="icon-top" src="<?php echo get_template_directory_uri(). '/images/icons/icon-mail.png'; ?>" alt="E-mail">
                    <a href="mailto:vendas@studiomobile.com.br" class="text-top">vendas@studiomobille.com.br</a>
                </div>
            </div>
            <div class="col-12 col-lg-2">
                <div class="social-items">
                    <a href="https://www.instagram.com/studiomobille19/ " target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/images/icons/icon-insta.png'  ?>" alt="Instagram" class="logo-social">
                    </a>
                    <a href="https://www.facebook.com/profile.php?id=100034595799688 " target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/images/icons/icon-face.png'  ?>" alt="Facebook" class="logo-social">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="outline-menu">
    </div>
    <div class="container-fluid">
        <div class="col-12 menu-items">
            <?php 
            wp_nav_menu([
                'theme_location' => 'header-menu',
                'menu_class' => 'options-menu',
                'container-class' => 'main-nav',
            ]);
            ?>

        </div>
    </div>
    <div class="container-fluid mobile-items">
        <div class="col-12 menu-items">
            <img class="logo-mobile" src="<?php echo get_template_directory_uri(). '/images/logo-mobile.png'; ?>">
            <div class="opened"></div>                    

            <div class="icon-menu-mobile">
            </div>
            <div class="icon-close-mobile">                        
            </div>

            <!--  Main menu - mobile -->
            <div class="outside-menu"></div>
            <div class="items-menu">
               <?php
               wp_nav_menu([
                'theme_location' => 'header-menu-mobile',
                'menu_class' => 'options-menu-mobile',
                'container-class' => 'main-nav',
            ]);

            ?>
        </div>
    </div>
</div>
</header>
