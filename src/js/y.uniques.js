// unique scripts go here.

function openPop(){
	$('.btn-showpop').click(function(){
		$('.pop-overlay').fadeIn();
		$('.form-pop-all').show();
		$('html, body').animate({
			scrollTop: 0
		}, 800);
		return false;
	});


	$('.close-pop').click(function(){
		$('.form-pop-all').fadeOut(400, function(){
			$('.pop-overlay').fadeOut('fast');
		});
	});

}


function menuMobile(){
	$('.icon-menu-mobile').click(function(){
		$('body, html').css('overflow-y', 'hidden');

		$('.icon-menu-mobile').hide();
			$('.icon-close-mobile').fadeIn();
		$('.outside-menu').fadeIn();	
		$('.items-menu').fadeIn();
		$('.sub-menu').addClass('menu-inactive');
	});

	$('.menu-item-77').click(function(){
		$('.sub-menu').removeClass('menu-inactive');
		$('.sub-menu').addClass('menu-active');
		$('.options-menu-mobile').addClass('menu-inactive');
	});


	$('.menu-item-110').click(function(){
		$('.options-menu-mobile').removeClass('menu-inactive');
		$('.sub-menu').removeClass('menu-active');
		$('.submenu').addClass('menu-inactive');
	});


	$('.icon-close-mobile').click(function(){
		$('.outside-menu').fadeOut();
		$('.icon-close-mobile').hide();
		$('.icon-menu-mobile').fadeIn();
		$('.options-menu-mobile').removeClass('menu-inactive');
		$('.sub-menu').addClass('menu-inactive');
		$('.items-menu').fadeOut();
		$('body, html').css('overflow-y', 'initial');
	});
}

function sliderItems(){
	$('.slider-mobile').slick({
		mobileFirst: true,
		arrows:false,
		dots:false,
		fade:true,
		centerMode:true,
		centerPadding:'60px',
		responsive:[{
			breakpoint:600,
			settings:{
				slidesToShow:1,
				sliderToScroll:1,
			},
			breakpoint:1200,
			settings:"unslick"
		}]
	})
}

function loadMask(){
	$('input[name="telefone"], input[name="celular"]').mask('(00) 00000-0000');
	$('.box-date').mask('00h00');
	$('.data').mask('00/00/0000');	
}

function loadMore(){
	$('.btn-more').click(function(){
		$('.box-more').fadeOut();
		$('.contentMore').fadeIn('fast');
	});
}

function linksPages(){
	$('.outline-image').click(function(){
		window.location = $(this).find("a:first").attr("href");
		return false;
	});

}