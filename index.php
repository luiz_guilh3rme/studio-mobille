<?php get_header(); ?>
<main class="structure">
	<section id="main-section">
		<div class="main-img"></div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 col-lg-5">
						<div class="text-main animated animatedFadeInUp fadeInUp">
							<h1>Excelência em <p class="strong-main">Móveis</p><p class="strong-main">Planejados</p></h1>
							<p class="desc-main">Com o objetivo de atender a todas necessidades do cliente com qualidade e perfeição, oferecemos uma consultoria antecipada com o Projetista, afim de se estudar todas as ideias, melhor distribuição dos móveis, cores, funcionalidade, matéria-prima, etc.</p>
						</div>
					</div>
				</div>
			</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 box-subtitles">
					<h2 class="sub-title">Ambientes</h2>
					<p class="desc-subtitle">Cada projeto é formado por soluções que representam um estilo de vida, uma forma de ver o mundo de se relacionar com o espaço onde se vive.</p>
				</div>
				<div class="col-12 col-md-3 col-lg-4 d-flex flex-column">
					<div class="img-big">
						<img class="img-fluid" src="<?php echo get_template_directory_uri().'/images/ambientes/cozinha.jpg'; ?>" alt="Cozinha">
						<img class="big-image icon-ambiente" src="<?php echo get_template_directory_uri(). '/images/icons/icon-cozinha-img.png' ?>" alt="Cozinha">
						<div class="outline-image">
							<p class="text-img txt-cozinha">Cozinha</p>
							<a class="link-image" href="cozinha"></a>
						</div>
					</div>
					<div class="img-small">
						<img class="img-fluid" src="<?php echo get_template_directory_uri().'/images/ambientes/quarto.jpg' ?>" alt="Quarto">
						<div class="small-image img-quarto icon-ambiente">
							<img src="<?php echo get_template_directory_uri(). '/images/icons/icon-quarto-img.png' ?>" alt="Quarto">
						</div>
						<div class="outline-image">
							<p class="text-img txt-quarto">Quarto</p>
							<a class="link-image" href="quarto"></a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-3 col-lg-4 d-flex flex-column">
					<div class="img-small">
						<img class="img-fluid" src="<?php echo get_template_directory_uri().'/images/ambientes/sala.jpg' ?>" alt="Espaço Living">
						<div class="mid-image-small icon-ambiente">
							<img class="icon-ambiente" src="<?php echo get_template_directory_uri(). '/images/icons/icon-living-img.png' ?>" alt="Sala">
						</div>
						<div class="col-12 outline-image mid-small">
							<p class="text-img txt-living">Living</p>
							<a class="link-image" href="sala"></a>
						</div>
					</div>
					<div class="img-big img-banheiro">
						<img class="img-fluid" src="<?php echo get_template_directory_uri().'/images/ambientes/banheiro.jpg'; ?>" alt="Banheiro">
						<div class="mid-image-big icon-ambiente">
							<img src="<?php echo get_template_directory_uri(). '/images/icons/icon-banheiro-img.png' ?>" alt="Banheiro">
						</div>
					</a>
					<div class="col-12 outline-image mid-out">
						<p class="text-img txt-banheiro">Banheiro</p>
						<a class="link-image" href="banheiro"></a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 col-lg-4 d-flex flex-column">
				<div class="img-big">
					<img class="img-fluid" src="<?php echo get_template_directory_uri().'/images/ambientes/escritorio.jpg'; ?>" alt="Escritório">
					<div class="big-image icon-ambiente">
						<img src="<?php echo get_template_directory_uri(). '/images/icons/icon-escritorio-img.png' ?>" alt="Escritório">
					</div>
					<div class="outline-image">
						<p class="text-img txt-escritorio">Escritório</p>
						<a class="link-image" href="escritorio"></a>
					</div>				
				</div>
				<div class="img-small">
					<img class="img-fluid" src="<?php echo get_template_directory_uri().'/images/ambientes/gourmet.jpg' ?>" alt="Espaço Gourmet">
					<div class="small-image icon-ambiente">
						<img src="<?php echo get_template_directory_uri(). '/images/icons/icon-gourmet-img.png' ?>" alt="Espaço Gourmet">
					</div>
					<div class="outline-image">
						<p class="text-img txt-gourmet">Gourmet</p>
						<a class="link-image" href="gourmet"></a>
					</div>
				</div>
			</div>	
			<div class="col-12">
				<div class="box-side-blue">
					<div class="text-box">
						<h3>Conheça nosso 
							<p>Showroom</p>
						</h3>
					</div>
				</div>
				<p class="desc-text-box">
					Venha visitar nosso Showroom, será um prazer recebe-lo.
					Entre em contato conosco e agende sua visita com nossos especialistas.
				</p>
				<div class="side-img">
					<img class="img-show" src="<?php echo get_template_directory_uri().'/images/showroom.jpg' ?>" alt="Espaço Showroom">
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</br></br>
<div class="about-us">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-lg-6">
				<div class="text-about">
					<h3>Sobre nós</h3>
					<p>A Studio Mobille é uma grife de móveis planejados que garante a qualidade em todos os detalhes para seu projeto, criada para oferecer um atendimento próximo e diferenciado a seus clientes, com projetos e uma linha de produtos alinhados com os desejos e necessidades do consumidor moderno.</p>

					<p>Somos uma marca nova que já nasce idônea, consolidando a experiência e dedicação de mais de 20 anos de experiência de seus fundadores no segmento de móveis planejados.</p>
					<div class="signature">
						<!--<p><strong>The boss</strong></p>
						<p>Sócio Fundador Studio Mobille.</p>-->
						<a href="sobre-nos"><button class="btn-saiba">Saiba Mais</button></a>
					</div>
				</div>	
			</div>
			<div class="col-12 col-lg-6">
				<div class="img-aside-persona">
					<img src="<?php echo get_template_directory_uri(). '/images/persona.png' ?>" alt="Saiba mais sobre a Studio Mobile">
				</div>
			</div>
		</div>
	</div>
</div>
</section>
</main>
<?php get_footer(); ?>