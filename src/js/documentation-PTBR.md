# Como isso funciona?

Cansei de editar arquivos **.js** com uma par de linha e que no fim não faziam tanto sentido, então dividi eles em vários documentos. 

os prefixos "a", "b" ou "z" são para ordem de concatenação

TUDO que aconteça provavelmente está sendo 'chamado' no arquivo **z.init.js** ou algo similar.

Isso não é melhor e nem feito de uma forma mais moderna tanto por preguiça, desconhecimento e falta de necessidade. Perdão aos JS ninjas.

## Ainda estou perdido :(

Caso precise de mais orentação dê uma olhada na **gulpfile.js** e no **readme.md** que estão na raiz do projeto. OU entre em contato comigo no github se você por acaso está cuidando de algum legacy code meu. 

[Drunksheep](http://github.com/drunksheep/) no [Github](http://github.com/)

--------------