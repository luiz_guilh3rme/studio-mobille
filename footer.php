<footer>
	<div class="footer-items">
		<div class="container-fluid">
			<div class="main-logo"></div>
			<div class="topics-footer">
				<div class="row">
					<div class="col-lg-3 item-footer">
						<p class="title-col">Sobre nós</p>
						<div class="line-title"></div>
						<div class="content-col-footer">
							<p>A Studio Mobille é uma grife de móveis planejados que garante a qualidade em todos os detalhes para seu projeto, criada para oferecer um atendimento próximo e diferenciado a seus clientes, com projetos e uma linha de produtos alinhados com os desejos e necessidades do consumidor moderno.</p>

							<p>Somos uma marca nova que já nasce idônea, consolidando a experiência e dedicação de mais de 20 anos de experiência de seus fundadores no segmento de móveis planejados.</p>
						</div>

						<a class="text-saibamais" href="sobre">Saiba Mais</a>
					</div>				
					<div class="col-lg-3 item-footer">
						<p class="title-col">Menu</p>
						<div class="line-title"></div>
						<?php 
						wp_nav_menu([
							'theme_location' => 'footer-menu',
							'menu_class' => 'footer-options-menu',
							'container-class' => 'items-footer',
						]);

						?>
					</div>	
					<div class="col-lg-3 item-footer">
						<p class="title-col">Ambientes</p>
						<div class="line-title"></div>
						<?php 
						wp_nav_menu([
							'theme_location' => 'ambientes-footer',
							'menu_class' => 'footer-options-menu',
							'container-class' => 'items-footer',
						]);

						?>
					</div>	
					<div class="col-lg-3 item-footer">
						<p class="title-col">Fale Conosco</p>
						<div class="line-title"></div>
						<div class="content-col-footer">
							<p>Av. Bosque da Saúde 1.061, Sala 101</p>
							<p>CEP: 04.142-090 São Paulo / SP</p>

							<p>
								<a class="phone-footer" href="tel:+551144101253">(11) 4410.1253</a>
							</p>
							<p>
								<a class="mail-footer" href="mailto:vendas@studiomobille.com.br">vendas@studiomobille.com.br</a></p>
						</div>

					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<div class="copyright-footer">
	<div class="container-fluid">
		<div class="row">
		<div class="col-12">
			<div class="text-end-footer d-flex flex-column flex-md-row align-items-center justify-content-md-between">
				<p class="text-footer-left"><strong>Studio Mobille</strong> - Todos os Direitos Reservados | ® Copyright 2019</p>
				<a href="https://www.3xceler.com.br/criacao-de-sites"><p class="text-footer-right">Criação de Sites: <img src="<?php echo get_template_directory_uri(). '/images/logo-3xceler.png' ?>" alt="Logotipo 3xceler"></p></a>					
			</div>
			</div>
		</div>
	</div>
</footer>
<?php 
get_template_part('includes/components/mobile-cta-wrapper');
get_template_part('includes/components/cta-wrapper');
get_template_part('includes/components/oldie'); 

wp_footer();
?>
</body>
</html> 
